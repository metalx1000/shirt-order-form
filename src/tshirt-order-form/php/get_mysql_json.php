<?php
include("mysql_header.php");
$table = "mytable";

$_GET = array_map('strip_tags', $_GET);
$_GET = array_map('htmlspecialchars', $_GET);

$result = mysqli_query($con,"SELECT * FROM $table");
$rows = array();
while($row = mysqli_fetch_array($result)) {
    $rows[] = $row;
}
print json_encode($rows);
mysqli_close($con);
?>
