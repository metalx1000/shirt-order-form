function validateForm() {
  let n = document.forms["form"]["name"].value;
  if (n == "") {
    alert("Name must be filled out");
    return false;
  }
  let e = document.forms["form"]["email"].value;
  if (e == "") {
    alert("Email must be filled out");
    return false;
  }
}
// example filter list 1
/*
let listItems = ["John Smith", "Sam Johnson", "Sally Sims", "Jack Packer", "Tim Sox"];
createFilter_1("list","theList", "items", listItems,"Filter Names...");
let items = document.querySelector("#list").querySelectorAll("li");
for( i of items ){
  i.classList.add("name");
  i.addEventListener("click", itemClick);
}


let colors = ["red","blue","green","yellow","purple"];
createFilter_1("colors","thecolors", "citems", colors, "Filter Colors...");
let coloritems = document.querySelector("#colors").querySelectorAll("li");
for( i of coloritems ){
  i.classList.add("colors");
  i.addEventListener("click", itemClick);
}

function itemClick(){
  console.log(this.innerText);
}
*/

// example request
/*
get('get.php', function(data){ 
  console.log(data); 
});
*/

/*
get('get_json.php', function(data){ 
  data = JASON.parse(data);
  for( d of data ){
    console.log(d);
  }
});
*/

// example request
/*
post('post.php', 'p1=1&p2=Hello+World', function(data){
  console.log(data); 
});
*/
// example request with data object
/*
post('post.php', { p1: 1, p2: 'Hello World' }, function(data){ 
  console.log(data); 
});
*/
