<?php
// Copyright (C) 2023 Kris Occhipinti
// https://filmsbykris.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation; 
// version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
// https://www.gnu.org/licenses/agpl-3.0.txt
//

$_POST = array_map('strip_tags', $_POST);
$_POST = array_map('htmlspecialchars', $_POST);

$name=$_POST['name'];
$email=$_POST['email'];
$small=$_POST['small'];
$medium=$_POST['medium'];
$large=$_POST['large'];
$xlarge=$_POST['xlarge'];
$xxlarge=$_POST['xxlarge'];

$db = new SQLite3('db/orders.db');
$db->exec("INSERT INTO orders('name','email','small','medium','large','xlarge','xxlarge','pickedup','paid') VALUES('$name','$email','$small','$medium','$large','$xlarge','$xxlarge',0,0)");


echo "<h1>Order Submitted</h1>";
?>

